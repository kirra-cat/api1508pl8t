/*
 *
 * dds.h
 *
 *  Created on: 29 jan. 2015
 *      Author: Sergey S.Sklyarov (kirra)
 */

#ifndef DDS_H_
#define DDS_H_

#include <stdint.h>

#define DDS_ADDR              (*(volatile uint16_t *)0x50000000)
#define DDS_DATA              (*(volatile uint16_t *)0x50000002)

#define DDS_PROFILE_0         0x0000
#define DDS_PROFILE_1         0x0001
#define DDS_PROFILE_2         0x0002
#define DDS_PROFILE_3         0x0003
#define DDS_PROFILE_4         0x0004
#define DDS_PROFILE_5         0x0005
#define DDS_PROFILE_6         0x0006
#define DDS_PROFILE_7         0x0007
#define DDS_PROFILE_8         0x0008
#define DDS_PROFILE_9         0x0009
#define DDS_PROFILE_10        0x000A
#define DDS_PROFILE_11        0x000B
#define DDS_PROFILE_12        0x000C
#define DDS_PROFILE_13        0x000D
#define DDS_PROFILE_14        0x000E
#define DDS_PROFILE_15        0x000F
#define DDS_PROFILE_16        0x0010
#define DDS_PROFILE_17        0x0011
#define DDS_PROFILE_18        0x0012
#define DDS_PROFILE_19        0x0013
#define DDS_PROFILE_20        0x0014
#define DDS_PROFILE_21        0x0015
#define DDS_PROFILE_22        0x0016
#define DDS_PROFILE_23        0x0017
#define DDS_PROFILE_24        0x0018
#define DDS_PROFILE_25        0x0019
#define DDS_PROFILE_26        0x001A
#define DDS_PROFILE_27        0x001B
#define DDS_PROFILE_28        0x001C
#define DDS_PROFILE_29        0x001D
#define DDS_PROFILE_30        0x001E
#define DDS_PROFILE_31        0x001F
#define DDS_PROFILE_32        0x0020
#define DDS_PROFILE_33        0x0021
#define DDS_PROFILE_34        0x0022
#define DDS_PROFILE_35        0x0023
#define DDS_PROFILE_36        0x0024
#define DDS_PROFILE_37        0x0025
#define DDS_PROFILE_38        0x0026
#define DDS_PROFILE_39        0x0027
#define DDS_PROFILE_40        0x0028
#define DDS_PROFILE_41        0x0029
#define DDS_PROFILE_42        0x002A
#define DDS_PROFILE_43        0x002B
#define DDS_PROFILE_44        0x002C
#define DDS_PROFILE_45        0x002D
#define DDS_PROFILE_46        0x002E
#define DDS_PROFILE_47        0x002F
#define DDS_PROFILE_48        0x0030
#define DDS_PROFILE_49        0x0031
#define DDS_PROFILE_50        0x0032
#define DDS_PROFILE_51        0x0033
#define DDS_PROFILE_52        0x0034
#define DDS_PROFILE_53        0x0035
#define DDS_PROFILE_54        0x0036
#define DDS_PROFILE_55        0x0037
#define DDS_PROFILE_56        0x0038
#define DDS_PROFILE_57        0x0039
#define DDS_PROFILE_58        0x003A
#define DDS_PROFILE_59        0x003B
#define DDS_PROFILE_60        0x003C
#define DDS_PROFILE_61        0x003D
#define DDS_PROFILE_62        0x003E
#define DDS_PROFILE_63        0x003F

#define DDS_SWRST             0x0000
#define DDS_DEVID             0x0001
#define DDS_SEL_REG           0x0002
#define DDS_CTR               0x0003
#define DDS_SYNC              0x0004
#define DDS_CLR               0x0005
#define DDS_LINK              0x0006
#define DDS_ROUTE             0x0007
#define DDS_TC_L              0x0008
#define DDS_TC_H              0x0009
#define DDS_T_CAPTURE         0x00E0
#define DDS_T_SEL_STATE       0x00E1
#define DDS_T_E_SEL           0x00E2

/** channel 1 */

#define DDS_CH1_LS_CTR        0x1000
#define DDS_CH1_LS_CRFMIN     0x1001
#define DDS_CH1_TSW           0x1002

#define DDS_CH1_LS_TPH1_L     0x1010
#define DDS_CH1_LS_TPH1_M     0x1011
#define DDS_CH1_LS_TPH1_H     0x1012

#define DDS_CH1_LS_TPH2_L     0x1014
#define DDS_CH1_LS_TPH2_M     0x1015
#define DDS_CH1_LS_TPH2_H     0x1016

#define DDS_CH1_LS_TPH3_L     0x1018
#define DDS_CH1_LS_TPH3_M     0x1019
#define DDS_CH1_LS_TPH3_H     0x101A

#define DDS_CH1_LS_TPH4_L     0x101C
#define DDS_CH1_LS_TPH4_M     0x101D
#define DDS_CH1_LS_TPH4_H     0x101E

#define DDS_CH1_LS_F1_L       0x1020
#define DDS_CH1_LS_F1_M       0x1021
#define DDS_CH1_LS_F1_H       0x1022

#define DDS_CH1_LS_F2_L       0x1024
#define DDS_CH1_LS_F2_M       0x1025
#define DDS_CH1_LS_F2_H       0x1026

#define DDS_CH1_LS_Ph1        0x1030
#define DDS_CH1_LS_Ph2        0x1031

#define DDS_CH1_LS_dF1_L      0x1040
#define DDS_CH1_LS_dF1_M      0x1041
#define DDS_CH1_LS_dF1_H      0x1042

#define DDS_CH1_LS_dF2_L      0x1044
#define DDS_CH1_LS_dF2_M      0x1045
#define DDS_CH1_LS_dF2_H      0x1046

#define DDS_CH1_dPh_all_L     0x1300
#define DDS_CH1_dPh_all_M     0x1301
#define DDS_CH1_dPh_all_H     0x1302

#define DDS_CH1_P_all         0x1304
#define DDS_CH1_Mul_all       0x1305
#define DDS_CH1_Offset_all    0x1306

#define DDS_CH1_dPh0_L        0x1400
#define DDS_CH1_dPh0_M        0x1401
#define DDS_CH1_dPh0_H        0x1402
#define DDS_CH1_P0            0x1404
#define DDS_CH1_Mul0          0x1405
#define DDS_CH1_Offset0       0x1406

#define DDS_CH1_dPh1_L        0x1410
#define DDS_CH1_dPh1_M        0x1411
#define DDS_CH1_dPh1_H        0x1412
#define DDS_CH1_P1            0x1414
#define DDS_CH1_Mul1          0x1415
#define DDS_CH1_Offset1       0x1416

#define DDS_CH1_dPh2_L        0x1420
#define DDS_CH1_dPh2_M        0x1421
#define DDS_CH1_dPh2_H        0x1422
#define DDS_CH1_P2            0x1424
#define DDS_CH1_Mul2          0x1425
#define DDS_CH1_Offset2       0x1426

#define DDS_CH1_dPh3_L        0x1430
#define DDS_CH1_dPh3_M        0x1431
#define DDS_CH1_dPh3_H        0x1432
#define DDS_CH1_P3            0x1434
#define DDS_CH1_Mul3          0x1435
#define DDS_CH1_Offset3       0x1436

#define DDS_CH1_dPh4_L        0x1440
#define DDS_CH1_dPh4_M        0x1441
#define DDS_CH1_dPh4_H        0x1442
#define DDS_CH1_P4            0x1444
#define DDS_CH1_Mul4          0x1445
#define DDS_CH1_Offset4       0x1446

#define DDS_CH1_dPh5_L        0x1450
#define DDS_CH1_dPh5_M        0x1451
#define DDS_CH1_dPh5_H        0x1452
#define DDS_CH1_P5            0x1454
#define DDS_CH1_Mul5          0x1455
#define DDS_CH1_Offset5       0x1456

#define DDS_CH1_dPh6_L        0x1460
#define DDS_CH1_dPh6_M        0x1461
#define DDS_CH1_dPh6_H        0x1462
#define DDS_CH1_P6            0x1464
#define DDS_CH1_Mul6          0x1465
#define DDS_CH1_Offset6       0x1466

#define DDS_CH1_dPh7_L        0x1470
#define DDS_CH1_dPh7_M        0x1471
#define DDS_CH1_dPh7_H        0x1472
#define DDS_CH1_P7            0x1474
#define DDS_CH1_Mul7          0x1475
#define DDS_CH1_Offset7       0x1476

#define DDS_CH1_dPh8_L        0x1480
#define DDS_CH1_dPh8_M        0x1481
#define DDS_CH1_dPh8_H        0x1482
#define DDS_CH1_P8            0x1484
#define DDS_CH1_Mul8          0x1485
#define DDS_CH1_Offset8       0x1486

#define DDS_CH1_dPh9_L        0x1490
#define DDS_CH1_dPh9_M        0x1491
#define DDS_CH1_dPh9_H        0x1492
#define DDS_CH1_P9            0x1494
#define DDS_CH1_Mul9          0x1495
#define DDS_CH1_Offset9       0x1496

#define DDS_CH1_dPh10_L        0x14A0
#define DDS_CH1_dPh10_M        0x14A1
#define DDS_CH1_dPh10_H        0x14A2
#define DDS_CH1_P10            0x14A4
#define DDS_CH1_Mul10          0x14A5
#define DDS_CH1_Offset10       0x14A6

#define DDS_CH1_dPh11_L        0x14B0
#define DDS_CH1_dPh11_M        0x14B1
#define DDS_CH1_dPh11_H        0x14B2
#define DDS_CH1_P11            0x14B4
#define DDS_CH1_Mul11          0x14B5
#define DDS_CH1_Offset11       0x14B6

#define DDS_CH1_dPh12_L        0x14C0
#define DDS_CH1_dPh12_M        0x14C1
#define DDS_CH1_dPh12_H        0x14C2
#define DDS_CH1_P12            0x14C4
#define DDS_CH1_Mul12          0x14C5
#define DDS_CH1_Offset12       0x14C6

#define DDS_CH1_dPh13_L        0x14D0
#define DDS_CH1_dPh13_M        0x14D1
#define DDS_CH1_dPh13_H        0x14D2
#define DDS_CH1_P13            0x14D4
#define DDS_CH1_Mul13          0x14D5
#define DDS_CH1_Offset13       0x14D6

#define DDS_CH1_dPh14_L        0x14E0
#define DDS_CH1_dPh14_M        0x14E1
#define DDS_CH1_dPh14_H        0x14E2
#define DDS_CH1_P14            0x14E4
#define DDS_CH1_Mul14          0x14E5
#define DDS_CH1_Offset14       0x14E6

#define DDS_CH1_dPh15_L        0x14F0
#define DDS_CH1_dPh15_M        0x14F1
#define DDS_CH1_dPh15_H        0x14F2
#define DDS_CH1_P15            0x14F4
#define DDS_CH1_Mul15          0x14F5
#define DDS_CH1_Offset15       0x14F6

#define DDS_CH1_dPh16_L        0x1500
#define DDS_CH1_dPh16_M        0x1501
#define DDS_CH1_dPh16_H        0x1502
#define DDS_CH1_P16            0x1504
#define DDS_CH1_Mul16          0x1505
#define DDS_CH1_Offset16       0x1506

#define DDS_CH1_dPh17_L        0x1510
#define DDS_CH1_dPh17_M        0x1511
#define DDS_CH1_dPh17_H        0x1512
#define DDS_CH1_P17            0x1514
#define DDS_CH1_Mul17          0x1515
#define DDS_CH1_Offset17       0x1516

#define DDS_CH1_dPh18_L        0x1520
#define DDS_CH1_dPh18_M        0x1521
#define DDS_CH1_dPh18_H        0x1522
#define DDS_CH1_P18            0x1524
#define DDS_CH1_Mul18          0x1525
#define DDS_CH1_Offset18       0x1526

#define DDS_CH1_dPh19_L        0x1530
#define DDS_CH1_dPh19_M        0x1531
#define DDS_CH1_dPh19_H        0x1532
#define DDS_CH1_P19            0x1534
#define DDS_CH1_Mul19          0x1535
#define DDS_CH1_Offset19       0x1536

#define DDS_CH1_dPh20_L        0x1540
#define DDS_CH1_dPh20_M        0x1541
#define DDS_CH1_dPh20_H        0x1542
#define DDS_CH1_P20            0x1544
#define DDS_CH1_Mul20          0x1545
#define DDS_CH1_Offset20       0x1546

#define DDS_CH1_dPh21_L        0x1550
#define DDS_CH1_dPh21_M        0x1551
#define DDS_CH1_dPh21_H        0x1552
#define DDS_CH1_P21            0x1554
#define DDS_CH1_Mul21          0x1555
#define DDS_CH1_Offset21       0x1556

#define DDS_CH1_dPh22_L        0x1560
#define DDS_CH1_dPh22_M        0x1561
#define DDS_CH1_dPh22_H        0x1562
#define DDS_CH1_P22            0x1564
#define DDS_CH1_Mul22          0x1565
#define DDS_CH1_Offset22       0x1566

#define DDS_CH1_dPh23_L        0x1570
#define DDS_CH1_dPh23_M        0x1571
#define DDS_CH1_dPh23_H        0x1572
#define DDS_CH1_P23            0x1574
#define DDS_CH1_Mul23          0x1575
#define DDS_CH1_Offset23       0x1576

#define DDS_CH1_dPh24_L        0x1580
#define DDS_CH1_dPh24_M        0x1581
#define DDS_CH1_dPh24_H        0x1582
#define DDS_CH1_P24            0x1584
#define DDS_CH1_Mul24          0x1585
#define DDS_CH1_Offset24       0x1586

#define DDS_CH1_dPh25_L        0x1590
#define DDS_CH1_dPh25_M        0x1591
#define DDS_CH1_dPh25_H        0x1592
#define DDS_CH1_P25            0x1594
#define DDS_CH1_Mul25          0x1595
#define DDS_CH1_Offset25       0x1596

#define DDS_CH1_dPh26_L        0x15A0
#define DDS_CH1_dPh26_M        0x15A1
#define DDS_CH1_dPh26_H        0x15A2
#define DDS_CH1_P26            0x15A4
#define DDS_CH1_Mul26          0x15A5
#define DDS_CH1_Offset26       0x15A6

#define DDS_CH1_dPh27_L        0x15B0
#define DDS_CH1_dPh27_M        0x15B1
#define DDS_CH1_dPh27_H        0x15B2
#define DDS_CH1_P27            0x15B4
#define DDS_CH1_Mul27          0x15B5
#define DDS_CH1_Offset27       0x15B6

#define DDS_CH1_dPh28_L        0x15C0
#define DDS_CH1_dPh28_M        0x15C1
#define DDS_CH1_dPh28_H        0x15C2
#define DDS_CH1_P28            0x15C4
#define DDS_CH1_Mul28          0x15C5
#define DDS_CH1_Offset28       0x15C6

#define DDS_CH1_dPh29_L        0x15D0
#define DDS_CH1_dPh29_M        0x15D1
#define DDS_CH1_dPh29_H        0x15D2
#define DDS_CH1_P29            0x15D4
#define DDS_CH1_Mul29          0x15D5
#define DDS_CH1_Offset29       0x15D6

#define DDS_CH1_dPh30_L        0x15E0
#define DDS_CH1_dPh30_M        0x15E1
#define DDS_CH1_dPh30_H        0x15E2
#define DDS_CH1_P30            0x15E4
#define DDS_CH1_Mul30          0x15E5
#define DDS_CH1_Offset30       0x15E6

#define DDS_CH1_dPh31_L        0x15F0
#define DDS_CH1_dPh31_M        0x15F1
#define DDS_CH1_dPh31_H        0x15F2
#define DDS_CH1_P31            0x15F4
#define DDS_CH1_Mul31          0x15F5
#define DDS_CH1_Offset31       0x15F6

#define DDS_CH1_dPh32_L        0x1600
#define DDS_CH1_dPh32_M        0x1601
#define DDS_CH1_dPh32_H        0x1602
#define DDS_CH1_P32            0x1604
#define DDS_CH1_Mul32          0x1605
#define DDS_CH1_Offset32       0x1606

#define DDS_CH1_dPh33_L        0x1610
#define DDS_CH1_dPh33_M        0x1611
#define DDS_CH1_dPh33_H        0x1612
#define DDS_CH1_P33            0x1614
#define DDS_CH1_Mul33          0x1615
#define DDS_CH1_Offset33       0x1616

#define DDS_CH1_dPh34_L        0x1620
#define DDS_CH1_dPh34_M        0x1621
#define DDS_CH1_dPh34_H        0x1622
#define DDS_CH1_P34            0x1624
#define DDS_CH1_Mul34          0x1625
#define DDS_CH1_Offset34       0x1626

#define DDS_CH1_dPh35_L        0x1630
#define DDS_CH1_dPh35_M        0x1631
#define DDS_CH1_dPh35_H        0x1632
#define DDS_CH1_P35            0x1634
#define DDS_CH1_Mul35          0x1635
#define DDS_CH1_Offset35       0x1636

#define DDS_CH1_dPh36_L        0x1640
#define DDS_CH1_dPh36_M        0x1641
#define DDS_CH1_dPh36_H        0x1642
#define DDS_CH1_P36            0x1644
#define DDS_CH1_Mul36          0x1645
#define DDS_CH1_Offset36       0x1646

#define DDS_CH1_dPh37_L        0x1650
#define DDS_CH1_dPh37_M        0x1651
#define DDS_CH1_dPh37_H        0x1652
#define DDS_CH1_P37            0x1654
#define DDS_CH1_Mul37          0x1655
#define DDS_CH1_Offset37       0x1656

#define DDS_CH1_dPh38_L        0x1660
#define DDS_CH1_dPh38_M        0x1661
#define DDS_CH1_dPh38_H        0x1662
#define DDS_CH1_P38            0x1664
#define DDS_CH1_Mul38          0x1665
#define DDS_CH1_Offset38       0x1666

#define DDS_CH1_dPh39_L        0x1670
#define DDS_CH1_dPh39_M        0x1671
#define DDS_CH1_dPh39_H        0x1672
#define DDS_CH1_P39            0x1674
#define DDS_CH1_Mul39          0x1675
#define DDS_CH1_Offset39       0x1676

#define DDS_CH1_dPh40_L        0x1680
#define DDS_CH1_dPh40_M        0x1681
#define DDS_CH1_dPh40_H        0x1682
#define DDS_CH1_P40            0x1684
#define DDS_CH1_Mul40          0x1685
#define DDS_CH1_Offset40       0x1686

#define DDS_CH1_dPh41_L        0x1690
#define DDS_CH1_dPh41_M        0x1691
#define DDS_CH1_dPh41_H        0x1692
#define DDS_CH1_P41            0x1694
#define DDS_CH1_Mul41          0x1695
#define DDS_CH1_Offset41       0x1696

#define DDS_CH1_dPh42_L        0x16A0
#define DDS_CH1_dPh42_M        0x16A1
#define DDS_CH1_dPh42_H        0x16A2
#define DDS_CH1_P42            0x16A4
#define DDS_CH1_Mul42          0x16A5
#define DDS_CH1_Offset42       0x16A6

#define DDS_CH1_dPh43_L        0x16B0
#define DDS_CH1_dPh43_M        0x16B1
#define DDS_CH1_dPh43_H        0x16B2
#define DDS_CH1_P43            0x16B4
#define DDS_CH1_Mul43          0x16B5
#define DDS_CH1_Offset43       0x16B6

#define DDS_CH1_dPh44_L        0x16C0
#define DDS_CH1_dPh44_M        0x16C1
#define DDS_CH1_dPh44_H        0x16C2
#define DDS_CH1_P44            0x16C4
#define DDS_CH1_Mul44          0x16C5
#define DDS_CH1_Offset44       0x16C6

#define DDS_CH1_dPh45_L        0x16D0
#define DDS_CH1_dPh45_M        0x16D1
#define DDS_CH1_dPh45_H        0x16D2
#define DDS_CH1_P45            0x16D4
#define DDS_CH1_Mul45          0x16D5
#define DDS_CH1_Offset45       0x16D6

#define DDS_CH1_dPh46_L        0x16E0
#define DDS_CH1_dPh46_M        0x16E1
#define DDS_CH1_dPh46_H        0x16E2
#define DDS_CH1_P46            0x16E4
#define DDS_CH1_Mul46          0x16E5
#define DDS_CH1_Offset46       0x16E6

#define DDS_CH1_dPh47_L        0x16F0
#define DDS_CH1_dPh47_M        0x16F1
#define DDS_CH1_dPh47_H        0x16F2
#define DDS_CH1_P47            0x16F4
#define DDS_CH1_Mul47          0x16F5
#define DDS_CH1_Offset47       0x16F6

#define DDS_CH1_dPh48_L        0x1700
#define DDS_CH1_dPh48_M        0x1701
#define DDS_CH1_dPh48_H        0x1702
#define DDS_CH1_P48            0x1704
#define DDS_CH1_Mul48          0x1705
#define DDS_CH1_Offset48       0x1706

#define DDS_CH1_dPh49_L        0x1710
#define DDS_CH1_dPh49_M        0x1711
#define DDS_CH1_dPh49_H        0x1712
#define DDS_CH1_P49            0x1714
#define DDS_CH1_Mul49          0x1715
#define DDS_CH1_Offset49       0x1716

#define DDS_CH1_dPh50_L        0x1720
#define DDS_CH1_dPh50_M        0x1721
#define DDS_CH1_dPh50_H        0x1722
#define DDS_CH1_P50            0x1724
#define DDS_CH1_Mul50          0x1725
#define DDS_CH1_Offset50       0x1726

#define DDS_CH1_dPh51_L        0x1730
#define DDS_CH1_dPh51_M        0x1731
#define DDS_CH1_dPh51_H        0x1732
#define DDS_CH1_P51            0x1734
#define DDS_CH1_Mul51          0x1735
#define DDS_CH1_Offset51       0x1736

#define DDS_CH1_dPh52_L        0x1740
#define DDS_CH1_dPh52_M        0x1741
#define DDS_CH1_dPh52_H        0x1742
#define DDS_CH1_P52            0x1744
#define DDS_CH1_Mul52          0x1745
#define DDS_CH1_Offset52       0x1746

#define DDS_CH1_dPh53_L        0x1750
#define DDS_CH1_dPh53_M        0x1751
#define DDS_CH1_dPh53_H        0x1752
#define DDS_CH1_P53            0x1754
#define DDS_CH1_Mul53          0x1755
#define DDS_CH1_Offset53       0x1756

#define DDS_CH1_dPh54_L        0x1760
#define DDS_CH1_dPh54_M        0x1761
#define DDS_CH1_dPh54_H        0x1762
#define DDS_CH1_P54            0x1764
#define DDS_CH1_Mul54          0x1765
#define DDS_CH1_Offset54       0x1766

#define DDS_CH1_dPh55_L        0x1770
#define DDS_CH1_dPh55_M        0x1771
#define DDS_CH1_dPh55_H        0x1772
#define DDS_CH1_P55            0x1774
#define DDS_CH1_Mul55          0x1775
#define DDS_CH1_Offset55       0x1776

#define DDS_CH1_dPh56_L        0x1780
#define DDS_CH1_dPh56_M        0x1781
#define DDS_CH1_dPh56_H        0x1782
#define DDS_CH1_P56            0x1784
#define DDS_CH1_Mul56          0x1785
#define DDS_CH1_Offset56       0x1786

#define DDS_CH1_dPh57_L        0x1790
#define DDS_CH1_dPh57_M        0x1791
#define DDS_CH1_dPh57_H        0x1792
#define DDS_CH1_P57            0x1794
#define DDS_CH1_Mul57          0x1795
#define DDS_CH1_Offset57       0x1796

#define DDS_CH1_dPh58_L        0x17A0
#define DDS_CH1_dPh58_M        0x17A1
#define DDS_CH1_dPh58_H        0x17A2
#define DDS_CH1_P58            0x17A4
#define DDS_CH1_Mul58          0x17A5
#define DDS_CH1_Offset58       0x17A6

#define DDS_CH1_dPh59_L        0x17B0
#define DDS_CH1_dPh59_M        0x17B1
#define DDS_CH1_dPh59_H        0x17B2
#define DDS_CH1_P59            0x17B4
#define DDS_CH1_Mul59          0x17B5
#define DDS_CH1_Offset59       0x17B6

#define DDS_CH1_dPh60_L        0x17C0
#define DDS_CH1_dPh60_M        0x17C1
#define DDS_CH1_dPh60_H        0x17C2
#define DDS_CH1_P60            0x17C4
#define DDS_CH1_Mul60          0x17C5
#define DDS_CH1_Offset60       0x17C6

#define DDS_CH1_dPh61_L        0x17D0
#define DDS_CH1_dPh61_M        0x17D1
#define DDS_CH1_dPh61_H        0x17D2
#define DDS_CH1_P61            0x17D4
#define DDS_CH1_Mul61          0x17D5
#define DDS_CH1_Offset61       0x17D6

#define DDS_CH1_dPh62_L        0x17E0
#define DDS_CH1_dPh62_M        0x17E1
#define DDS_CH1_dPh62_H        0x17E2
#define DDS_CH1_P62            0x17E4
#define DDS_CH1_Mul62          0x17E5
#define DDS_CH1_Offset62       0x17E6

#define DDS_CH1_dPh63_L        0x17F0
#define DDS_CH1_dPh63_M        0x17F1
#define DDS_CH1_dPh63_H        0x17F2
#define DDS_CH1_P63            0x17F4
#define DDS_CH1_Mul63          0x17F5
#define DDS_CH1_Offset63       0x17F6

#define DDS_CH1_T_dPh_L        0x1800
#define DDS_CH1_T_dPh_M        0x1801
#define DDS_CH1_T_dPh_H        0x1802

#define DDS_CH1_T_P            0x1804
#define DDS_CH1_T_Mul          0x1805
#define DDS_CH1_T_Offset       0x1806
#define DDS_CH1_T_SEL          0x1808
#define DDS_CH1_T_out1         0x1810
#define DDS_CH1_T_out2         0x1811
#define DDS_CH1_T_out3         0x1812
#define DDS_CH1_T_out4         0x1813

/** channel 2 */

#define DDS_CH2_LS_CTR        0x2000
#define DDS_CH2_LS_CRFMIN     0x2001
#define DDS_CH2_TSW           0x2002

#define DDS_CH2_LS_TPH1_L     0x2010 /** bits[15:0]  */
#define DDS_CH2_LS_TPH1_M     0x2011 /** bits[31:16] */
#define DDS_CH2_LS_TPH1_H     0x2012 /** bits[45:32] */

#define DDS_CH2_LS_TPH2_L     0x2014
#define DDS_CH2_LS_TPH2_M     0x2015
#define DDS_CH2_LS_TPH2_H     0x2016

#define DDS_CH2_LS_TPH3_L     0x2018
#define DDS_CH2_LS_TPH3_M     0x2019
#define DDS_CH2_LS_TPH3_H     0x201A

#define DDS_CH2_LS_TPH4_L     0x201C
#define DDS_CH2_LS_TPH4_M     0x201D
#define DDS_CH2_LS_TPH4_H     0x201E

#define DDS_CH2_LS_F1_L       0x2020
#define DDS_CH2_LS_F1_M       0x2021
#define DDS_CH2_LS_F1_H       0x2022

#define DDS_CH2_LS_F2_L       0x2024
#define DDS_CH2_LS_F2_M       0x2025
#define DDS_CH2_LS_F2_H       0x2026

#define DDS_CH2_LS_Ph1        0x2030
#define DDS_CH2_LS_Ph2        0x2031

#define DDS_CH2_LS_dF1_L      0x2040
#define DDS_CH2_LS_dF1_M      0x2041
#define DDS_CH2_LS_dF1_H      0x2042

#define DDS_CH2_LS_dF2_L      0x2044
#define DDS_CH2_LS_dF2_M      0x2045
#define DDS_CH2_LS_dF2_H      0x2046

#define DDS_CH2_dPh_all_L     0x2300
#define DDS_CH2_dPh_all_M     0x2301
#define DDS_CH2_dPh_all_H     0x2302

#define DDS_CH2_P_all         0x2304
#define DDS_CH2_Mul_all       0x2305
#define DDS_CH2_Offset_all    0x2306
#define DDS_CH2_dPh0_L        0x2400
#define DDS_CH2_dPh0_M        0x2401
#define DDS_CH2_dPh0_H        0x2402
#define DDS_CH2_P0            0x2404
#define DDS_CH2_Mul0          0x2405
#define DDS_CH2_Offset0       0x2406

#define DDS_CH2_dPh1_L        0x2410
#define DDS_CH2_dPh1_M        0x2411
#define DDS_CH2_dPh1_H        0x2412
#define DDS_CH2_P1            0x2414
#define DDS_CH2_Mul1          0x2415
#define DDS_CH2_Offset1       0x2416

#define DDS_CH2_dPh2_L        0x2420
#define DDS_CH2_dPh2_M        0x2421
#define DDS_CH2_dPh2_H        0x2422
#define DDS_CH2_P2            0x2424
#define DDS_CH2_Mul2          0x2425
#define DDS_CH2_Offset2       0x2426

#define DDS_CH2_dPh3_L        0x2430
#define DDS_CH2_dPh3_M        0x2431
#define DDS_CH2_dPh3_H        0x2432
#define DDS_CH2_P3            0x2434
#define DDS_CH2_Mul3          0x2435
#define DDS_CH2_Offset3       0x2436

#define DDS_CH2_dPh4_L        0x2440
#define DDS_CH2_dPh4_M        0x2441
#define DDS_CH2_dPh4_H        0x2442
#define DDS_CH2_P4            0x2444
#define DDS_CH2_Mul4          0x2445
#define DDS_CH2_Offset4       0x2446

#define DDS_CH2_dPh5_L        0x2450
#define DDS_CH2_dPh5_M        0x2451
#define DDS_CH2_dPh5_H        0x2452
#define DDS_CH2_P5            0x2454
#define DDS_CH2_Mul5          0x2455
#define DDS_CH2_Offset5       0x2456

#define DDS_CH2_dPh6_L        0x2460
#define DDS_CH2_dPh6_M        0x2461
#define DDS_CH2_dPh6_H        0x2462
#define DDS_CH2_P6            0x2464
#define DDS_CH2_Mul6          0x2465
#define DDS_CH2_Offset6       0x2466

#define DDS_CH2_dPh7_L        0x2470
#define DDS_CH2_dPh7_M        0x2471
#define DDS_CH2_dPh7_H        0x2472
#define DDS_CH2_P7            0x2474
#define DDS_CH2_Mul7          0x2475
#define DDS_CH2_Offset7       0x2476

#define DDS_CH2_dPh8_L        0x2480
#define DDS_CH2_dPh8_M        0x2481
#define DDS_CH2_dPh8_H        0x2482
#define DDS_CH2_P8            0x2484
#define DDS_CH2_Mul8          0x2485
#define DDS_CH2_Offset8       0x2486

#define DDS_CH2_dPh9_L        0x2490
#define DDS_CH2_dPh9_M        0x2491
#define DDS_CH2_dPh9_H        0x2492
#define DDS_CH2_P9            0x2494
#define DDS_CH2_Mul9          0x2495
#define DDS_CH2_Offset9       0x2496

#define DDS_CH2_dPh10_L        0x24A0
#define DDS_CH2_dPh10_M        0x24A1
#define DDS_CH2_dPh10_H        0x24A2
#define DDS_CH2_P10            0x24A4
#define DDS_CH2_Mul10          0x24A5
#define DDS_CH2_Offset10       0x24A6

#define DDS_CH2_dPh11_L        0x24B0
#define DDS_CH2_dPh11_M        0x24B1
#define DDS_CH2_dPh11_H        0x24B2
#define DDS_CH2_P11            0x24B4
#define DDS_CH2_Mul11          0x24B5
#define DDS_CH2_Offset11       0x24B6

#define DDS_CH2_dPh12_L        0x24C0
#define DDS_CH2_dPh12_M        0x24C1
#define DDS_CH2_dPh12_H        0x24C2
#define DDS_CH2_P12            0x24C4
#define DDS_CH2_Mul12          0x24C5
#define DDS_CH2_Offset12       0x24C6

#define DDS_CH2_dPh13_L        0x24D0
#define DDS_CH2_dPh13_M        0x24D1
#define DDS_CH2_dPh13_H        0x24D2
#define DDS_CH2_P13            0x24D4
#define DDS_CH2_Mul13          0x24D5
#define DDS_CH2_Offset13       0x24D6

#define DDS_CH2_dPh14_L        0x24E0
#define DDS_CH2_dPh14_M        0x24E1
#define DDS_CH2_dPh14_H        0x24E2
#define DDS_CH2_P14            0x24E4
#define DDS_CH2_Mul14          0x24E5
#define DDS_CH2_Offset14       0x24E6

#define DDS_CH2_dPh15_L        0x24F0
#define DDS_CH2_dPh15_M        0x24F1
#define DDS_CH2_dPh15_H        0x24F2
#define DDS_CH2_P15            0x24F4
#define DDS_CH2_Mul15          0x24F5
#define DDS_CH2_Offset15       0x24F6

#define DDS_CH2_dPh16_L        0x2500
#define DDS_CH2_dPh16_M        0x2501
#define DDS_CH2_dPh16_H        0x2502
#define DDS_CH2_P16            0x2504
#define DDS_CH2_Mul16          0x2505
#define DDS_CH2_Offset16       0x2506

#define DDS_CH2_dPh17_L        0x2510
#define DDS_CH2_dPh17_M        0x2511
#define DDS_CH2_dPh17_H        0x2512
#define DDS_CH2_P17            0x2514
#define DDS_CH2_Mul17          0x2515
#define DDS_CH2_Offset17       0x2516

#define DDS_CH2_dPh18_L        0x2520
#define DDS_CH2_dPh18_M        0x2521
#define DDS_CH2_dPh18_H        0x2522
#define DDS_CH2_P18            0x2524
#define DDS_CH2_Mul18          0x2525
#define DDS_CH2_Offset18       0x2526

#define DDS_CH2_dPh19_L        0x2530
#define DDS_CH2_dPh19_M        0x2531
#define DDS_CH2_dPh19_H        0x2532
#define DDS_CH2_P19            0x2534
#define DDS_CH2_Mul19          0x2535
#define DDS_CH2_Offset19       0x2536

#define DDS_CH2_dPh20_L        0x2540
#define DDS_CH2_dPh20_M        0x2541
#define DDS_CH2_dPh20_H        0x2542
#define DDS_CH2_P20            0x2544
#define DDS_CH2_Mul20          0x2545
#define DDS_CH2_Offset20       0x2546

#define DDS_CH2_dPh21_L        0x2550
#define DDS_CH2_dPh21_M        0x2551
#define DDS_CH2_dPh21_H        0x2552
#define DDS_CH2_P21            0x2554
#define DDS_CH2_Mul21          0x2555
#define DDS_CH2_Offset21       0x2556

#define DDS_CH2_dPh22_L        0x2560
#define DDS_CH2_dPh22_M        0x2561
#define DDS_CH2_dPh22_H        0x2562
#define DDS_CH2_P22            0x2564
#define DDS_CH2_Mul22          0x2565
#define DDS_CH2_Offset22       0x2566

#define DDS_CH2_dPh23_L        0x2570
#define DDS_CH2_dPh23_M        0x2571
#define DDS_CH2_dPh23_H        0x2572
#define DDS_CH2_P23            0x2574
#define DDS_CH2_Mul23          0x2575
#define DDS_CH2_Offset23       0x2576

#define DDS_CH2_dPh24_L        0x2580
#define DDS_CH2_dPh24_M        0x2581
#define DDS_CH2_dPh24_H        0x2582
#define DDS_CH2_P24            0x2584
#define DDS_CH2_Mul24          0x2585
#define DDS_CH2_Offset24       0x2586

#define DDS_CH2_dPh25_L        0x2590
#define DDS_CH2_dPh25_M        0x2591
#define DDS_CH2_dPh25_H        0x2592
#define DDS_CH2_P25            0x2594
#define DDS_CH2_Mul25          0x2595
#define DDS_CH2_Offset25       0x2596

#define DDS_CH2_dPh26_L        0x25A0
#define DDS_CH2_dPh26_M        0x25A1
#define DDS_CH2_dPh26_H        0x25A2
#define DDS_CH2_P26            0x25A4
#define DDS_CH2_Mul26          0x25A5
#define DDS_CH2_Offset26       0x25A6

#define DDS_CH2_dPh27_L        0x25B0
#define DDS_CH2_dPh27_M        0x25B1
#define DDS_CH2_dPh27_H        0x25B2
#define DDS_CH2_P27            0x25B4
#define DDS_CH2_Mul27          0x25B5
#define DDS_CH2_Offset27       0x25B6

#define DDS_CH2_dPh28_L        0x25C0
#define DDS_CH2_dPh28_M        0x25C1
#define DDS_CH2_dPh28_H        0x25C2
#define DDS_CH2_P28            0x25C4
#define DDS_CH2_Mul28          0x25C5
#define DDS_CH2_Offset28       0x25C6

#define DDS_CH2_dPh29_L        0x25D0
#define DDS_CH2_dPh29_M        0x25D1
#define DDS_CH2_dPh29_H        0x25D2
#define DDS_CH2_P29            0x25D4
#define DDS_CH2_Mul29          0x25D5
#define DDS_CH2_Offset29       0x25D6

#define DDS_CH2_dPh30_L        0x25E0
#define DDS_CH2_dPh30_M        0x25E1
#define DDS_CH2_dPh30_H        0x25E2
#define DDS_CH2_P30            0x25E4
#define DDS_CH2_Mul30          0x25E5
#define DDS_CH2_Offset30       0x25E6

#define DDS_CH2_dPh31_L        0x25F0
#define DDS_CH2_dPh31_M        0x25F1
#define DDS_CH2_dPh31_H        0x25F2
#define DDS_CH2_P31            0x25F4
#define DDS_CH2_Mul31          0x25F5
#define DDS_CH2_Offset31       0x25F6

#define DDS_CH2_dPh32_L        0x2600
#define DDS_CH2_dPh32_M        0x2601
#define DDS_CH2_dPh32_H        0x2602
#define DDS_CH2_P32            0x2604
#define DDS_CH2_Mul32          0x2605
#define DDS_CH2_Offset32       0x2606

#define DDS_CH2_dPh33_L        0x2610
#define DDS_CH2_dPh33_M        0x2611
#define DDS_CH2_dPh33_H        0x2612
#define DDS_CH2_P33            0x2614
#define DDS_CH2_Mul33          0x2615
#define DDS_CH2_Offset33       0x2616

#define DDS_CH2_dPh34_L        0x2620
#define DDS_CH2_dPh34_M        0x2621
#define DDS_CH2_dPh34_H        0x2622
#define DDS_CH2_P34            0x2624
#define DDS_CH2_Mul34          0x2625
#define DDS_CH2_Offset34       0x2626

#define DDS_CH2_dPh35_L        0x2630
#define DDS_CH2_dPh35_M        0x2631
#define DDS_CH2_dPh35_H        0x2632
#define DDS_CH2_P35            0x2634
#define DDS_CH2_Mul35          0x2635
#define DDS_CH2_Offset35       0x2636

#define DDS_CH2_dPh36_L        0x2640
#define DDS_CH2_dPh36_M        0x2641
#define DDS_CH2_dPh36_H        0x2642
#define DDS_CH2_P36            0x2644
#define DDS_CH2_Mul36          0x2645
#define DDS_CH2_Offset36       0x2646

#define DDS_CH2_dPh37_L        0x2650
#define DDS_CH2_dPh37_M        0x2651
#define DDS_CH2_dPh37_H        0x2652
#define DDS_CH2_P37            0x2654
#define DDS_CH2_Mul37          0x2655
#define DDS_CH2_Offset37       0x2656

#define DDS_CH2_dPh38_L        0x2660
#define DDS_CH2_dPh38_M        0x2661
#define DDS_CH2_dPh38_H        0x2662
#define DDS_CH2_P38            0x2664
#define DDS_CH2_Mul38          0x2665
#define DDS_CH2_Offset38       0x2666

#define DDS_CH2_dPh39_L        0x2670
#define DDS_CH2_dPh39_M        0x2671
#define DDS_CH2_dPh39_H        0x2672
#define DDS_CH2_P39            0x2674
#define DDS_CH2_Mul39          0x2675
#define DDS_CH2_Offset39       0x2676

#define DDS_CH2_dPh40_L        0x2680
#define DDS_CH2_dPh40_M        0x2681
#define DDS_CH2_dPh40_H        0x2682
#define DDS_CH2_P40            0x2684
#define DDS_CH2_Mul40          0x2685
#define DDS_CH2_Offset40       0x2686

#define DDS_CH2_dPh41_L        0x2690
#define DDS_CH2_dPh41_M        0x2691
#define DDS_CH2_dPh41_H        0x2692
#define DDS_CH2_P41            0x2694
#define DDS_CH2_Mul41          0x2695
#define DDS_CH2_Offset41       0x2696

#define DDS_CH2_dPh42_L        0x26A0
#define DDS_CH2_dPh42_M        0x26A1
#define DDS_CH2_dPh42_H        0x26A2
#define DDS_CH2_P42            0x26A4
#define DDS_CH2_Mul42          0x26A5
#define DDS_CH2_Offset42       0x26A6

#define DDS_CH2_dPh43_L        0x26B0
#define DDS_CH2_dPh43_M        0x26B1
#define DDS_CH2_dPh43_H        0x26B2
#define DDS_CH2_P43            0x26B4
#define DDS_CH2_Mul43          0x26B5
#define DDS_CH2_Offset43       0x26B6

#define DDS_CH2_dPh44_L        0x26C0
#define DDS_CH2_dPh44_M        0x26C1
#define DDS_CH2_dPh44_H        0x26C2
#define DDS_CH2_P44            0x26C4
#define DDS_CH2_Mul44          0x26C5
#define DDS_CH2_Offset44       0x26C6

#define DDS_CH2_dPh45_L        0x26D0
#define DDS_CH2_dPh45_M        0x26D1
#define DDS_CH2_dPh45_H        0x26D2
#define DDS_CH2_P45            0x26D4
#define DDS_CH2_Mul45          0x26D5
#define DDS_CH2_Offset45       0x26D6

#define DDS_CH2_dPh46_L        0x26E0
#define DDS_CH2_dPh46_M        0x26E1
#define DDS_CH2_dPh46_H        0x26E2
#define DDS_CH2_P46            0x26E4
#define DDS_CH2_Mul46          0x26E5
#define DDS_CH2_Offset46       0x26E6

#define DDS_CH2_dPh47_L        0x26F0
#define DDS_CH2_dPh47_M        0x26F1
#define DDS_CH2_dPh47_H        0x26F2
#define DDS_CH2_P47            0x26F4
#define DDS_CH2_Mul47          0x26F5
#define DDS_CH2_Offset47       0x26F6

#define DDS_CH2_dPh48_L        0x2700
#define DDS_CH2_dPh48_M        0x2701
#define DDS_CH2_dPh48_H        0x2702
#define DDS_CH2_P48            0x2704
#define DDS_CH2_Mul48          0x2705
#define DDS_CH2_Offset48       0x2706

#define DDS_CH2_dPh49_L        0x2710
#define DDS_CH2_dPh49_M        0x2711
#define DDS_CH2_dPh49_H        0x2712
#define DDS_CH2_P49            0x2714
#define DDS_CH2_Mul49          0x2715
#define DDS_CH2_Offset49       0x2716

#define DDS_CH2_dPh50_L        0x2720
#define DDS_CH2_dPh50_M        0x2721
#define DDS_CH2_dPh50_H        0x2722
#define DDS_CH2_P50            0x2724
#define DDS_CH2_Mul50          0x2725
#define DDS_CH2_Offset50       0x2726

#define DDS_CH2_dPh51_L        0x2730
#define DDS_CH2_dPh51_M        0x2731
#define DDS_CH2_dPh51_H        0x2732
#define DDS_CH2_P51            0x2734
#define DDS_CH2_Mul51          0x2735
#define DDS_CH2_Offset51       0x2736

#define DDS_CH2_dPh52_L        0x2740
#define DDS_CH2_dPh52_M        0x2741
#define DDS_CH2_dPh52_H        0x2742
#define DDS_CH2_P52            0x2744
#define DDS_CH2_Mul52          0x2745
#define DDS_CH2_Offset52       0x2746

#define DDS_CH2_dPh53_L        0x2750
#define DDS_CH2_dPh53_M        0x2751
#define DDS_CH2_dPh53_H        0x2752
#define DDS_CH2_P53            0x2754
#define DDS_CH2_Mul53          0x2755
#define DDS_CH2_Offset53       0x2756

#define DDS_CH2_dPh54_L        0x2760
#define DDS_CH2_dPh54_M        0x2761
#define DDS_CH2_dPh54_H        0x2762
#define DDS_CH2_P54            0x2764
#define DDS_CH2_Mul54          0x2765
#define DDS_CH2_Offset54       0x2766

#define DDS_CH2_dPh55_L        0x2770
#define DDS_CH2_dPh55_M        0x2771
#define DDS_CH2_dPh55_H        0x2772
#define DDS_CH2_P55            0x2774
#define DDS_CH2_Mul55          0x2775
#define DDS_CH2_Offset55       0x2776

#define DDS_CH2_dPh56_L        0x2780
#define DDS_CH2_dPh56_M        0x2781
#define DDS_CH2_dPh56_H        0x2782
#define DDS_CH2_P56            0x2784
#define DDS_CH2_Mul56          0x2785
#define DDS_CH2_Offset56       0x2786

#define DDS_CH2_dPh57_L        0x2790
#define DDS_CH2_dPh57_M        0x2791
#define DDS_CH2_dPh57_H        0x2792
#define DDS_CH2_P57            0x2794
#define DDS_CH2_Mul57          0x2795
#define DDS_CH2_Offset57       0x2796

#define DDS_CH2_dPh58_L        0x27A0
#define DDS_CH2_dPh58_M        0x27A1
#define DDS_CH2_dPh58_H        0x27A2
#define DDS_CH2_P58            0x27A4
#define DDS_CH2_Mul58          0x27A5
#define DDS_CH2_Offset58       0x27A6

#define DDS_CH2_dPh59_L        0x27B0
#define DDS_CH2_dPh59_M        0x27B1
#define DDS_CH2_dPh59_H        0x27B2
#define DDS_CH2_P59            0x27B4
#define DDS_CH2_Mul59          0x27B5
#define DDS_CH2_Offset59       0x27B6

#define DDS_CH2_dPh60_L        0x27C0
#define DDS_CH2_dPh60_M        0x27C1
#define DDS_CH2_dPh60_H        0x27C2
#define DDS_CH2_P60            0x27C4
#define DDS_CH2_Mul60          0x27C5
#define DDS_CH2_Offset60       0x27C6

#define DDS_CH2_dPh61_L        0x27D0
#define DDS_CH2_dPh61_M        0x27D1
#define DDS_CH2_dPh61_H        0x27D2
#define DDS_CH2_P61            0x27D4
#define DDS_CH2_Mul61          0x27D5
#define DDS_CH2_Offset61       0x27D6

#define DDS_CH2_dPh62_L        0x27E0
#define DDS_CH2_dPh62_M        0x27E1
#define DDS_CH2_dPh62_H        0x27E2
#define DDS_CH2_P62            0x27E4
#define DDS_CH2_Mul62          0x27E5
#define DDS_CH2_Offset62       0x27E6

#define DDS_CH2_dPh63_L        0x27F0
#define DDS_CH2_dPh63_M        0x27F1
#define DDS_CH2_dPh63_H        0x27F2
#define DDS_CH2_P63            0x27F4
#define DDS_CH2_Mul63          0x27F5
#define DDS_CH2_Offset63       0x27F6

#define DDS_CH2_T_dPh_L        0x2800
#define DDS_CH2_T_dPh_M        0x2801
#define DDS_CH2_T_dPh_H        0x2802

#define DDS_CH2_T_P            0x2804
#define DDS_CH2_T_Mul          0x2805
#define DDS_CH2_T_Offset       0x2806
#define DDS_CH2_T_SEL          0x2808
#define DDS_CH2_T_out1         0x2810
#define DDS_CH2_T_out2         0x2811
#define DDS_CH2_T_out3         0x2812
#define DDS_CH2_T_out4         0x2813

/** Значения амплитуды, сдвига фаз и напряжения для режима ЛЧМ в стадии Sx */
#define DDS_CH1_LFM_S1_Mul     0x1415
#define DDS_CH1_LFM_S1_P       0x1414
#define DDS_CH1_LFM_S1_Offset  0x1416

#define DDS_CH1_LFM_S2_Mul     0x1425
#define DDS_CH1_LFM_S2_P       0x1424
#define DDS_CH1_LFM_S2_Offset  0x1426

#define DDS_CH1_LFM_S3_Mul     0x1435
#define DDS_CH1_LFM_S3_P       0x1434
#define DDS_CH1_LFM_S3_Offset  0x1436

#define DDS_CH1_LFM_S4_Mul     0x1405
#define DDS_CH1_LFM_S4_P       0x1404
#define DDS_CH1_LFM_S4_Offset  0x1406

#define DDS_CH2_LFM_S1_Mul     0x2425
#define DDS_CH2_LFM_S1_P       0x2424
#define DDS_CH2_LFM_S1_Offset  0x2426

#define DDS_CH2_LFM_S2_Mul     0x2425
#define DDS_CH2_LFM_S2_P       0x2424
#define DDS_CH2_LFM_S2_Offset  0x2426

#define DDS_CH2_LFM_S3_Mul     0x2435
#define DDS_CH2_LFM_S3_P       0x2434
#define DDS_CH2_LFM_S3_Offset  0x2436

#define DDS_CH2_LFM_S4_Mul     0x2405
#define DDS_CH2_LFM_S4_P       0x2404
#define DDS_CH2_LFM_S4_Offset  0x2406

#define DDS_CH1_LFM_STAGE_1    0x43600014
#define DDS_CH1_LFM_STAGE_2    0x43600018
#define DDS_CH1_LFM_STAGE_3    0x4360001C
#define DDS_CH1_LFM_STAGE_4    0x43600020

#define DDS_CH2_LFM_STAGE_1    0x4390001C
#define DDS_CH2_LFM_STAGE_2    0x43900020
#define DDS_CH2_LFM_STAGE_3    0x43900024
#define DDS_CH2_LFM_STAGE_4    0x43900028

inline void dds_software_reset(void);

inline void dds_write(uint16_t addr, uint16_t data);
inline uint16_t dds_read(uint16_t addr);

inline void dds_set_profile_ch1(uint8_t data);
inline void dds_set_profile_ch2(uint8_t data);

inline void dds_set_lfm_stage(uint32_t stage);

#endif

/*
 * end file dds.h
 */
