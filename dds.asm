/*
 *
 * dds.asm
 *
 *  Created on: 29 june 2015
 *      Author: Sergey S.Sklyarov (kirra)
 */

.text
  .syntax unified
  .global dds_write
  .global dds_read
  .global dds_set_profile_ch1
  .global dds_set_profile_ch2
  .global dds_software_reset
  .global dds_set_lfm_stage

  .type dds_write,           function
  .type dds_read,            function
  .type dds_set_profile_ch1, function
  .type dds_set_profile_ch2, function
  .type dds_software_reset,  function
  .type dds_set_lfm_stage,   function

  .equ DDS_ADDR,              0x50000000
  .equ DDS_DATA,              0x50000002

  .equ ADDRESS_PORT_SEL1_0,   0x43600014 /** port address to SEL1[0] */
  .equ ADDRESS_PORT_SEL1_1,   0x43600018 /** port address to SEL1[1] */
  .equ ADDRESS_PORT_SEL1_2,   0x4360001C /** port address to SEL1[2] */
  .equ ADDRESS_PORT_SEL1_3,   0x43600020 /** port address to SEL1[3] */
  .equ ADDRESS_PORT_SEL1_4,   0x43600024 /** port address to SEL1[4] */
  .equ ADDRESS_PORT_SEL1_5,   0x43600028 /** port address to SEL1[5] */

  .equ ADDRESS_PORT_SEL2_0,   0x43600014 /** port address to SEL2[0] */
  .equ ADDRESS_PORT_SEL2_1,   0x43900020 /** port address to SEL2[1] */
  .equ ADDRESS_PORT_SEL2_2,   0x43900024 /** port address to SEL2[2] */
  .equ ADDRESS_PORT_SEL2_3,   0x43900028 /** port address to SEL2[3] */
  .equ ADDRESS_PORT_SEL2_4,   0x4390002C /** port address to SEL2[4] */
  .equ ADDRESS_PORT_SEL2_5,   0x43D00020 /** port address to SEL2[5] */

  .thumb

  dds_write:

  push   { r4, r5 }

  ldr    r2,  =DDS_ADDR
  ldr    r3,  =DDS_DATA

  strh   r0,  [r2]
  strh   r1,  [r3]

  pop    { r4, r5 }
  bx     lr

  dds_read:

  push   { r4, r5 }

  ldr    r4,  =DDS_ADDR
  ldr    r5,  =DDS_DATA

  strh   r0,  [r4]
  ldrh   r0,  [r5]

  pop    { r4, r5 }
  bx     lr

  dds_set_profile_ch1:

  push   { r1, r5, r6, r7, r8, r9, r10 }

  ldr    r5,  =ADDRESS_PORT_SEL1_0
  ldr    r6,  =ADDRESS_PORT_SEL1_1
  ldr    r7,  =ADDRESS_PORT_SEL1_2
  ldr    r8,  =ADDRESS_PORT_SEL1_3
  ldr    r9,  =ADDRESS_PORT_SEL1_4
  ldr    r10, =ADDRESS_PORT_SEL1_5

  and    r1,  r0,  #1
  str    r1,  [r5]

  ubfx   r1,  r0,  #1,  #1
  str    r1,  [r6]

  ubfx   r1,  r0,  #2,  #1
  str    r1,  [r7]

  ubfx   r1,  r0,  #3,  #1
  str    r1,  [r8]

  ubfx   r1,  r0,  #4,  #1
  str    r1,  [r9]

  ubfx   r1,  r0,  #5,  #1
  str    r1,  [r10]

  pop    { r1, r5, r6, r7, r8, r9, r10 }

  bx     lr

  dds_set_profile_ch2:

  push   { r1, r5, r6, r7, r8, r9, r10 }

  ldr    r5,  =ADDRESS_PORT_SEL2_0
  ldr    r6,  =ADDRESS_PORT_SEL2_1
  ldr    r7,  =ADDRESS_PORT_SEL2_2
  ldr    r8,  =ADDRESS_PORT_SEL2_3
  ldr    r9,  =ADDRESS_PORT_SEL2_4
  ldr    r10, =ADDRESS_PORT_SEL2_5

  and    r1,  r0,  #1
  str    r1,  [r5]

  ubfx   r1,  r0,  #1,  #1
  str    r1,  [r6]

  ubfx   r1,  r0,  #2,  #1
  str    r1,  [r7]

  ubfx   r1,  r0,  #3,  #1
  str    r1,  [r8]

  ubfx   r1,  r0,  #4,  #1
  str    r1,  [r9]

  ubfx   r1,  r0,  #5,  #1
  str    r1,  [r10]

  pop    { r1, r5, r6, r7, r8, r9, r10 }

  bx     lr

  dds_software_reset:

  push   { r4, r5, r6, r7 }

  ldr    r4,  =DDS_ADDR
  ldr    r5,  =DDS_DATA

  movw   r6,  #0x0000
  movw   r7,  #0x0078

  strh   r6,  [r4]
  strh   r7,  [r5]

  pop    { r4, r5, r6, r7 }

  bx     lr

  dds_set_lfm_stage:

  push   { r2 }

  movw   r2,  #1
  str    r2,  [r0]

  movw   r2,  #0
  str    r2,  [r0]

  pop    { r2 }

  bx     lr

.end


/**
 * end file dds.asm
 */
